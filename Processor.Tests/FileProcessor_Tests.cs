using System.Collections.Generic;
using Xunit;

namespace Processor.Tests
{
    public class FileProcessor_Tests
    {

        [Fact]
        public void CheckEncoding_ReturnTrue()
        {
            var fp = new FileProcessor("Files\\dbUtf.txt");
            bool result = fp.IsUTF();
            Assert.True(result, "����������� ���� ����� ���������� ���������, ��������� ������ ���� True");
        }

        [Fact]
        public void CheckProcessingResult()
        {
            var expectedResult = new List<string>() { "�����������", "�����������" };

            var fp = new FileProcessor("Files\\dbUtf.txt");
            var result = fp.ProcessFile();

            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void CheckRemoveSpecialCharacters()
        {
            var fp = new FileProcessor("Files\\dbUtf.txt");
            string result = fp.RemoveSpecialCharacters("��������\r\n@_123");
            Assert.Equal("��������", result);
        }
    }
}