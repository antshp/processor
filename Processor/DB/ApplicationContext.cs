﻿using Microsoft.EntityFrameworkCore;

namespace Processor
{

    /// <summary>
    /// Класс контекста данных
    /// </summary>
    public class ApplicationContext : DbContext
    {
        public DbSet<Word> Words { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=.\\db\\main.db");
        }

    }
}