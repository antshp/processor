﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor
{

    /// <summary>
    /// Модель описывающая таблицу words
    /// </summary>
    public class Word
    {

        public int id { get; set; }
        public string text;
        public int rate;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public int Rate
        {
            get { return rate; }
            set { rate = value; }
        }

        public Word(string text, int rate = 1)
        {
            this.text = text;
            this.rate = rate;
        }

    }
}
