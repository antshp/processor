﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor
{
    class Database
    {

        ApplicationContext db;
        int WordsCount = 5;

        public Database()
        {
            db = new ApplicationContext();
        }

        /// <summary>
        /// Метод создания словаря.
        /// </summary>
        /// <param name="newWords">Список строк-слов</param>
        public void Add(List<string> newWords)
        {
            List<Word> words = new List<Word>();
            AddWords(words, newWords);
        }

        /// <summary>
        /// Метод обновления словаря, т.е. добавления новых слов без удаления старых.
        /// </summary>
        /// <param name="newWords">Список строк-слов</param>
        public void Update(List<string> newWords)
        {
            List<Word> words = db.Words.ToList();
            AddWords(words, newWords);
        }

        /// <summary>
        /// Метод добавления новых слов в базу данных.
        /// </summary>
        /// <param name="words">Слова, уже имеющиеся в словаре</param>
        /// <param name="newWords">Слова, которые необходимо добавить в словарь</param>
        void AddWords(List<Word> words, List<string> newWords)
        {

            foreach (string word in newWords)
            {
                var wordInDB = words.Where(w => w.Text == word).FirstOrDefault();
                if (wordInDB != null)
                    wordInDB.Rate++;
                else
                    words.Add(new Word(word));
            }

            Clear();
            db.Words.AddRange(words);
            db.SaveChanges();

            var wordsWithLowRate = words.Where(w => w.Rate < 3);
            db.Words.RemoveRange(wordsWithLowRate);

            db.SaveChanges();
        }

        /// <summary>
        /// Метод очистки базы данных от всех слов.
        /// </summary>
        public void Clear()
        {
            db.Words.RemoveRange(db.Words);
            db.SaveChanges();
        }

        /// <summary>
        /// Метод получения списка слов из словаря по ключевому слову.
        /// </summary>
        /// <param name="partOfWord">Поисковая фраза</param>
        /// <returns>Список слов</returns>
        public List<Word> GetWords(string partOfWord)
        {
            return db.Words
                .Where(w => w.Text.StartsWith(partOfWord))
                .OrderByDescending(w => w.Rate)
                .ThenBy(w => w.Text)
                .Take(WordsCount).ToList();
        }

    }
}
