﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using System.Globalization;

namespace Processor
{
    public class FileProcessor
    {

        string FilePath;

        int MinWordLength = 3;
        int MaxWordLength = 15;

        public FileProcessor(string FilePath)
        {
            if (!File.Exists(FilePath))
                throw new Exception("Указанный файл недоступен");

            this.FilePath = FilePath;

            if (!IsUTF())
                throw new Exception("Указанный файл имеет кодировку, отличную от UTF8 (With BOM)");

            
        }

        /// <summary>
        /// Метод проверки соответствия кодировки файла кодировке UTF8
        /// </summary>
        /// <returns>True в случае соответствия</returns>
        public bool IsUTF()
        {
            var bom = new byte[4];
            using (var file = new FileStream(FilePath, FileMode.Open, FileAccess.Read))
            {
                file.Read(bom, 0, 4);
            }

            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Метод обработки файла.
        /// Содержимое файла присваивается строке, далее происходит удаление лишних символов
        /// и приведение к нижнему регистру.
        /// Получившаяся строка делится на отдельные слова по разделителю-пробелу.
        /// </summary>
        /// <returns>Список строк-слов</returns>
        public List<string> ProcessFile()
        {
            List<string> result = new List<string>();
            string text = File.ReadAllText(FilePath);
            text = RemoveSpecialCharacters(text).ToLower();
            string[] wordsArray = text.Split(" ");
            foreach (string word in wordsArray)
            {
                if (word.Length >= MinWordLength && word.Length <= MaxWordLength)
                    result.Add(word);
            }
                
            return result;
        }

        /// <summary>
        /// Метод удаления из строки всех символов, кроме букв и пробелов.
        /// </summary>
        /// <param name="str">Строка, которую необходимо обработать</param>
        /// <returns>Обработанная строка</returns>
        public string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= 'А' && c <= 'Я') || (c >= 'а' && c <= 'я') || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

    }
}
