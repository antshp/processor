﻿using Microsoft.Extensions.CommandLineUtils;
using System;
using System.Text;

namespace Processor
{
    class Program
    {

        /// <summary>
        /// Точка входа в приложение.
        /// В случае наличия команд вызывает метод, обрабатывающий их.
        /// Далее ждет ввода ключевой фразы и осуществляет поиск по ней в базе данных.
        /// </summary>
        /// <param name="args">Консольные команды управления словарем</param>
        static void Main(string[] args)
        {

            if (args.Length > 0 && args.Length <= 2)
            {
                try
                {
                    CommandLineApplication CLA = CreateCLA(args);
                    CLA.Execute(args);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Ошибка: " + e.Message);
                }
            }

            while (true)
            {
                try
                {
                    Console.WriteLine("Введите слово или часть слова:");

                    string searchRequest = readLineWithCancel();

                    var db = new Database();
                    var result = db.GetWords(searchRequest);

                    foreach (Word word in result)
                        Console.WriteLine(word.Text);

                }
                catch (Exception e)
                {
                    Console.WriteLine("Ошибка: " + e.Message);
                }
            }

        }

        /// <summary>
        /// Метод для обработки указанных пользователем команд.
        /// Обрабатываются только команды Create, Update и Clear
        /// </summary>
        /// <param name="args">Консольные команды управления словарем</param>
        static CommandLineApplication CreateCLA(string[] args)
        {
            CommandLineApplication commandLineApplication = new CommandLineApplication(throwOnUnexpectedArg: false);

            CommandOption createOption = commandLineApplication.Option(
                "--create",
                "Создание словаря",
                CommandOptionType.SingleValue
                );
            CommandOption updateOption = commandLineApplication.Option(
                "--update",
                "Обновление словаря",
                CommandOptionType.SingleValue
                );
            CommandOption clearOption = commandLineApplication.Option(
                "--clear",
                "Очистка словаря",
                CommandOptionType.NoValue
                );
            commandLineApplication.HelpOption("--help");

            commandLineApplication.OnExecute(() =>
            {
                var db = new Database();

                if (createOption.HasValue())
                {
                    var fp = new FileProcessor(createOption.Value());
                    db.Add(fp.ProcessFile());
                    Console.WriteLine("Словарь создан");
                }

                if (updateOption.HasValue())
                {
                    var fp = new FileProcessor(updateOption.Value());
                    db.Update(fp.ProcessFile());
                    Console.WriteLine("Словарь обновлен");
                }

                if (clearOption.HasValue())
                {
                    db.Clear();
                    Console.WriteLine("Словарь очищен");
                }

                return 1;
            });

            return commandLineApplication;
        }

        /// <summary>
        /// Метод обработки введенного пользователем текста.
        /// Закрывает приложение при нажатии на Esc или Enter + пустая строка.
        /// Передает введенную строку если она не пустая при нажатии на Enter.
        /// </summary>
        /// <returns>Введенная пользователем поисковая фраза</returns>
        static string readLineWithCancel()
        {
            string result = "";

            StringBuilder buffer = new StringBuilder();
            ConsoleKeyInfo button = Console.ReadKey(true);

            while (button.Key != ConsoleKey.Enter && button.Key != ConsoleKey.Escape)
            {
                Console.Write(button.KeyChar);
                buffer.Append(button.KeyChar);
                button = Console.ReadKey(true);
            }

            if(button.Key == ConsoleKey.Escape || (button.Key == ConsoleKey.Enter && buffer.Length == 0))
                Environment.Exit(0);
            else if (button.Key == ConsoleKey.Enter)
            {
                result = buffer.ToString().ToLower();
                Console.WriteLine();
            }

            return result;
        }
    }
}